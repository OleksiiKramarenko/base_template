// базовые переменные
const path = require('path')
const webpack = require('webpack')

// переменные для плагинов
const ExtractTextPlugin = require('extract-text-webpack-plugin')

// настройка модуля
module.exports = {
	// базовый путь к проекту
	context: path.resolve(__dirname, 'scr'),

	// точка входа 
	entry: {
		// основные файл
		app: [
			'./js/app.js',
			'./scss/style.scss'
		],
	},

	// точка выхода
	output: {
		filename: 'js/[name].js',
		path: path.resolve(__dirname, 'dist'),
		publicPath: '../'
	},

	// Настройки devServer
	devServer: {
		contentBase: './app'
	},

	module: {
		rules: [
			// scss
			{
				test: /\.scss$/,
				use: ExtractTextPlugin.extract({
					use: [{
						loader: "sass-loader",
						option: { sourceMap: true }
					}, {
						loader: "css-loader",
						option: { sourceMap: true }
					}, ],

				}),
			},
		],
	},

	plugins: [
		new ExtractTextPlugin(
			"./css/style.css"
		),
	],

};
